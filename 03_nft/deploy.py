import configparser
import io

# Load the configuration file
with open(".env",'r') as f:
    exec(f.readline())
    exec(f.readline())
assert ALCHEMY_API_KEY_URL
assert RINKEBY_PRIVATE_KEY
print(ALCHEMY_API_KEY_URL)
print(RINKEBY_PRIVATE_KEY)

import json
with open('./artifacts/contracts/NFTee.sol/GameItem.json') as f:
    loaded_contract = json.loads(f.read())

import sys
sys.path.append('../../../')
from blockchainservices.core import Ethnet
args = {'geth_url':ALCHEMY_API_KEY_URL}
e = Ethnet(args)
e.connect()
abi = loaded_contract['abi']
bytecode = loaded_contract['bytecode']
        
transaction  = {'from':'0xAbeCC1969EE5e19e4A53Def763E1779d1edf10a9',
                       'abi':abi,
                        #'args':[1],
                        'bytecode':bytecode,
                        'private_key':RINKEBY_PRIVATE_KEY,
                       }
print(transaction)
res =e.deploy_smart_contract(transaction)
print(res)
