// Address of the Whitelist Contract that you deployed
const WHITELIST_CONTRACT_ADDRESS = "0x31E49131F183078f0A308Ac88cFb3C979f43D31a";
// URL to extract Metadata for a Crypto Dev NFT
const METADATA_URL = "https://jg-hype-onboarding.vercel.app/api/";

module.exports = { WHITELIST_CONTRACT_ADDRESS, METADATA_URL };